
// TODO remove all medipack/hunting_rifle again (add convar for that)
// TODO other weapons/melee? (https://developer.valvesoftware.com/wiki/Category:Left_4_Dead_2_Entities)
// TODO give ammo to weapon according to convar?

#define DEBUG 1

#pragma semicolon 1

#include <sourcemod>
#include <sdktools>

public Plugin:myinfo = {
	name = "U-WUT Saferoom Items",
	author = "Jojo le Barjos",
	description = "Spawn desired items in saferoom.",
	version = "1.3"
};

enum ItemId {
	ITEM_FIRST_AID_KIT,
	ITEM_DEFIBRILLATOR,
	ITEM_PAIN_PILLS,
	ITEM_ADRENALINE,
	ITEM_PIPE_BOMB,
	ITEM_MOLOTOV,
	ITEM_VOMITJAR,
    ITEM_PISTOL,
    ITEM_PISTOL_MAGNUM,
	ITEM_HUNTING_RIFLE,
    ITEM_RIFLE,
    ITEM_RIFLE_AK47,
    ITEM_RIFLE_DESERT,
    ITEM_AUTOSHOTGUN,
    ITEM_SHOTGUN_SPAS,
    ITEM_SNIPER_MILITARY,
	ITEM_MACHETE,
	ITEM_KNIFE
};

new const String:g_sItemNames[ItemId][] = {
	"weapon_first_aid_kit",
	"weapon_defibrillator",
	"weapon_pain_pills",
	"weapon_adrenaline",
	"weapon_pipe_bomb",
	"weapon_molotov",
	"weapon_vomitjar",
	"weapon_pistol",
	"weapon_pistol_magnum",
	"weapon_hunting_rifle",
    "weapon_rifle",
    "weapon_rifle_ak47",
    "weapon_rifle_desert",
    "weapon_autoshotgun",
    "weapon_shotgun_spas",
    "weapon_sniper_military",
	"weapon_melee",
	"weapon_melee"
};

new const String:g_sItemMeleeNames[ItemId][] = {
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
	"",
    "",
    "",
    "",
    "",
    "",
    "",
	"machete",
	"knife"
};

new const g_iItemAmmoCounts[ItemId] = {
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	150,
    360,
    360,
    360,
    90,
    90,
    150,
	-1,
	-1
};

new const String:g_sItemFriendlyNames[ItemId][] = {
	"first aid kit",
	"defibrillator",
	"pain pills",
	"adrenaline",
	"pipe bomb",
	"molotov",
	"vomitjar",
    "pistol",
    "pistol magnum",
	"hunting rifle",
	"assault rifle",
	"ak47 rifle",
	"desert rifle",
	"auto shotgun",
	"combat shotgun",
	"sniper military",
	"machete",
	"knife"
};

new const String:g_sItemConvarNames[ItemId][] = {
	"uwut_first_aid_kit",
	"uwut_defibrillator",
	"uwut_pain_pills",
	"uwut_adrenaline",
	"uwut_pipe_bomb",
	"uwut_molotov",
	"uwut_vomitjar",
	"uwut_pistol",
	"uwut_pistol_magnum",
	"uwut_hunting_rifle",
	"uwut_rifle",
	"uwut_rifle_ak47",
	"uwut_rifle_desert",
	"uwut_autoshotgun",
	"uwut_shotgun_spas",
	"uwut_sniper_military",
	"uwut_machete",
	"uwut_knife"
};

new Handle:g_hItemConvars[ItemId];
new g_iItemRemaining[ItemId];

new bool:g_bNewRound;

public OnPluginStart() {
	for (new i = 0; i < ItemId; ++i) {
		new String:desc[128];
		Format(desc, 127, "Number of %s to spawn", g_sItemFriendlyNames[i]);
		g_hItemConvars[i] = CreateConVar(g_sItemConvarNames[i], "0", desc, FCVAR_PLUGIN);
	}
	g_bNewRound = false;
	HookEvent("round_start", Event_RoundStart);
	CreateTimer(1.0, Timer_SpawnItems, INVALID_HANDLE, TIMER_REPEAT);
#if DEBUG
	RegAdminCmd("uwut_spawn", Spawn_Cmd, ADMFLAG_BAN, "Spawn items");
#endif
	SetRemainingItems(true);
}

public Action:Event_RoundStart(Handle:event, const String:name[], bool:dontBroadcast) {
	g_bNewRound = true;
	return Plugin_Continue;
}

public Action:Timer_SpawnItems(Handle:timer) {
	if (g_bNewRound) {
#if DEBUG
		PrintToChatAll("UWUT detected round start");
#endif
		g_bNewRound = false;
		SetRemainingItems();
	}
	SpawnRemainingItems();
}

#if DEBUG
public Action:Spawn_Cmd(client, args) {
	SetRemainingItems();
	return Plugin_Handled;
}
#endif

static SetRemainingItems(bool:clear = false) {
	for (new i = 0; i < ItemId; ++i)
		g_iItemRemaining[i] = clear ? 0 : GetConVarInt(g_hItemConvars[i]);
}

static SpawnRemainingItems() {
	new total = 0;
	for (new i = 0; i < ItemId; ++i)
		total += g_iItemRemaining[i];
	if (total == 0)
		return;
#if DEBUG
	PrintToChatAll("UWUT spawning items...");
#endif
	new success = 0;
	for (new i = 0; i < ItemId; ++i) {
		new tmp = SpawnNextItems(i, g_iItemRemaining[i]);
		g_iItemRemaining[i] -= tmp;
		success += tmp;
	}
#if DEBUG
	PrintToChatAll("UWUT %d/%d items spawned", success, total);
#endif
}

static SpawnNextItems(ItemId:id, count) {
	new success = 0;
	for (new i = 0; i < count; ++i)
		if (SpawnNextItem(id))
			++success;
	return success;
}

static bool:SpawnNextItem(ItemId id) {
	new Float:location[3];
	if (!GetNextLocation(id, location)) {
#if DEBUG
	  PrintToChatAll("UWUT failed to find location for %s", g_sItemFriendlyNames[id]);
#endif
		return false;
  }
	return SpawnItem(id, location);
}

static bool:SpawnItem(ItemId:id, const Float:location[3]) {
	new entity = CreateEntityByName(g_sItemNames[id]);
	if (entity < 0) {
#if DEBUG
    PrintToChatAll("UWUT failed to spawn %s", g_sItemFriendlyNames[id]);
#endif
		return false;
  }
	TeleportEntity(entity, location, NULL_VECTOR, NULL_VECTOR);
	if (!StrEqual(g_sItemMeleeNames[id], ""))
		DispatchKeyValue(entity, "melee_script_name", g_sItemMeleeNames[id]);
	DispatchKeyValue(entity, "solid", "6");
	DispatchSpawn(entity);
	if (g_iItemAmmoCounts[id] >= 0)
		SetEntProp(entity, Prop_Send, "m_iExtraPrimaryAmmo", g_iItemAmmoCounts[id], 4);
#if DEBUG
	PrintToChatAll("UWUT %s spawned at %f %f %f", g_sItemFriendlyNames[id], location[0], location[1], location[2]);
#endif
	return true;
}

static bool:GetNextLocation(ItemId:id, Float:location[3]) {
	new client = GetNextSurvivor();
	if (client == 0)
		return false;
	GetClientAbsOrigin(client, location);
	location[0] += GetURandomFloat() * 8 - 4;
	location[1] += GetURandomFloat() * 8 - 4;
	location[2] += GetURandomFloat() * 8 + 22;
	return true;
}

static GetNextSurvivor() {
	static old = 0;
	for (new i = 1; i <= MaxClients; i++) {
		new client = old + i;
		while (client > MaxClients)
			client -= MaxClients;
		if (IsClientConnected(client) && IsClientInGame(client) && IsPlayerAlive(client) && GetClientTeam(client) == 2) {
			old = client;
			return client;
		}
	}
	old = 0;
	return 0;
}
