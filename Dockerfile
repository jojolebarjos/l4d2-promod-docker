FROM ubuntu:16.04

ENV STEAMCMD_FOLDER /opt/steamcmd
ENV STEAMCMD ${STEAMCMD_FOLDER}/steamcmd.sh

ENV GAME_NAME left4dead2
ENV GAME_ID 222860
ENV GAME_FOLDER ${STEAMCMD_FOLDER}/${GAME_NAME}

RUN \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        ca-certificates \
        curl \
        lib32gcc1 \
        lib32stdc++6 \
        lib32z1 \
        lib32z1-dev \
        locales && \
    locale-gen en_US && \
    locale-gen en_US.UTF-8 && \
    update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8

RUN \
    mkdir -p ${STEAMCMD_FOLDER} && \
    curl -s http://media.steampowered.com/installer/steamcmd_linux.tar.gz | tar -v -C ${STEAMCMD_FOLDER} -zx

RUN \
    ${STEAMCMD} \
        +force_install_dir ${GAME_FOLDER} \
        +login anonymous \
        +app_update ${GAME_ID} validate \
        +quit

WORKDIR ${GAME_FOLDER}

RUN \
    mkdir -p /root/.steam/sdk32/ && \
    ln -s ${STEAMCMD_FOLDER}/linux32/steamclient.so /root/.steam/sdk32/

COPY L4D2-Competitive-Rework ./left4dead2/

RUN \
    mv ./left4dead2/cfg/server.cfg ./left4dead2/cfg/server_original.cfg

COPY left4dead2 ./left4dead2/

COPY start.sh .

RUN chmod +x ./start.sh

ENTRYPOINT ["./start.sh"]

CMD ["+sv_lan", "1", "+map", "c2m1_highway"]
